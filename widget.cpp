#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QTranslator>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(ui->comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(onChanged(int)));

}

Widget::~Widget()
{
    delete ui;
}

void Widget::onChanged(int index)
{
    qDebug()<<index;
    QTranslator translator;
    if(index == 0){
        translator.load(":/translations/en.qm") ;
    }else{
        translator.load(":/translations/cn.qm") ;
    }
    qApp->installTranslator(&translator);
    Widget::updateUI();
}

/* 更新UI */
void Widget::updateUI()
{
    ui->label->setText(tr("Hello,world"));

}

