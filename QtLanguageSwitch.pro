#-------------------------------------------------
#
# Project created by QtCreator 2016-12-17T16:55:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtLanguageSwitch
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui

TRANSLATIONS = translations/en.ts \
               translations/cn.ts \

RESOURCES += \
    res.qrc
